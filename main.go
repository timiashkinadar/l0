package main

import (
	"context"
	"github.com/go-chi/chi/v5"
	_ "github.com/lib/pq"
	_ "github.com/nats-io/stan.go"
	_ "github.com/nats-io/stan.go/pb"
	"gitlab.com/timiashkinadar/l0/config"
	"gitlab.com/timiashkinadar/l0/handler"
	"gitlab.com/timiashkinadar/l0/repository"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	cfg := config.GetConfig()

	repo, db := repository.NewRepository(cfg)
	defer db.Close()

	cache, err := repository.NewCache(repo)
	if err != nil {
		log.Fatal(err)
	}

	natsHandler, conn := handler.NewHandler(repo, cache, cfg.Nats)
	defer conn.Close()

	err = natsHandler.StartHandle()
	if err != nil {
		log.Fatal(err)
	}

	r := chi.NewRouter()
	orderHandler := handler.NewHttpOrderHandler(cache)

	r.Post("/order", orderHandler.OrderUI)
	r.Get("/order", orderHandler.OrderUI)

	server := &http.Server{
		Addr:    cfg.Server.Port,
		Handler: r,
	}

	go func() {
		log.Printf("Starting server on %s", server.Addr)
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("ListenAndServe(): %s", err)
		}
	}()

	stopAppCh := make(chan struct{})
	go gracefulShutdown(server, stopAppCh)

	<-stopAppCh
}

func gracefulShutdown(srv *http.Server, stopAppCh chan struct{}) {
	sgnl := make(chan os.Signal)
	signal.Notify(sgnl, syscall.SIGINT, syscall.SIGTERM)

	log.Println("Captured signal: ", <-sgnl)
	log.Println("Gracefully shutting down server...")

	if err := srv.Shutdown(context.Background()); err != nil {
		log.Println("Can't shutdown main server: ", err.Error())
	}
	stopAppCh <- struct{}{}
}

package config

import (
	"github.com/spf13/viper"
	"log"
)

type Cfg struct {
	Postgres PostgresCfg
	Nats     NatsCfg
	Server   Server
}
type NatsCfg struct {
	ClusterID string
	ClientID  string
	Subject   string
	URL       string
}

type PostgresCfg struct {
	User   string
	Passwd string
	DbName string
	Host   string
	Port   int
}

type Server struct {
	Port string
}

func GetConfig() *Cfg {
	viper.SetConfigName("config")   // Имя файла конфигурации (без расширения)
	viper.SetConfigType("yaml")     // Тип файла конфигурации (yaml, json, toml и др.)
	viper.AddConfigPath("./config") // Путь к файлу конфигурации (можно указать несколько путей)

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatalf("Failed to read config file: %v", err)
	}

	var cfg Cfg

	err = viper.Unmarshal(&cfg)
	if err != nil {
		log.Fatalf("Failed to unmarshal config file: %v", err)
	}

	return &cfg
}

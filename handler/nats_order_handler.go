package handler

import (
	"fmt"
	"github.com/nats-io/stan.go"
	"gitlab.com/timiashkinadar/l0/config"
	"gitlab.com/timiashkinadar/l0/model"
	"gitlab.com/timiashkinadar/l0/repository"
	"log"
)

type NatsHandler interface {
	HandleMessage(data []byte) error
	StartHandle() error
}

type NatsOrderHandler struct {
	repo  repository.Repository
	cache repository.Cache
	cfg   config.NatsCfg
	conn  stan.Conn
}

func NewHandler(repo repository.Repository, cache repository.Cache, cfg config.NatsCfg) (NatsHandler, stan.Conn) {
	sc, err := stan.Connect(cfg.ClusterID, cfg.ClientID, stan.NatsURL(cfg.URL))
	if err != nil {
		log.Fatal(err)
	}

	return &NatsOrderHandler{repo: repo, cache: cache, cfg: cfg, conn: sc}, sc
}

func (h *NatsOrderHandler) HandleMessage(data []byte) error {
	order, err := model.UnmarshalOrder(data)
	if err != nil {
		return fmt.Errorf("failed to unmarshal message from subject - %s: %w", h.cfg.Subject, err)
	}

	err = h.repo.SaveOrder(order)
	if err != nil {
		return fmt.Errorf("failed to save to message from subject - %s to db: %w", h.cfg.Subject, err)
	}

	h.cache.SaveOrder(order)

	return nil
}

func (h *NatsOrderHandler) StartHandle() error {
	_, err := h.conn.Subscribe(h.cfg.Subject, func(msg *stan.Msg) {
		err := h.HandleMessage(msg.Data)
		if err != nil {
			log.Println("Failed to handle message:", err)

		}

		msg.Ack()
	}, stan.SetManualAckMode())

	if err != nil {
		return fmt.Errorf("failed to subscribe to subject - %s: %w", h.cfg.Subject, err)
	}

	log.Println("Subscribed to subject:", h.cfg.Subject)
	return nil
}

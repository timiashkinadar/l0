package handler

import (
	"gitlab.com/timiashkinadar/l0/model"
	"gitlab.com/timiashkinadar/l0/repository"
	"html/template"
	"net/http"
)

type HttpHandler interface {
	OrderUI(w http.ResponseWriter, r *http.Request)
}

type HttpOrderHandler struct {
	cache repository.Cache
}

func NewHttpOrderHandler(cache repository.Cache) HttpHandler {
	return &HttpOrderHandler{cache: cache}
}

type ViewData struct {
	Order model.Order
	Error error
}

func (h *HttpOrderHandler) OrderUI(w http.ResponseWriter, r *http.Request) {
	var data ViewData
	var err error
	tmpl := template.Must(template.New("order").Parse(Template))

	if r.Method == "POST" {
		orderUID := r.FormValue("uid")
		data.Order, data.Error = h.cache.GetByID(orderUID)
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

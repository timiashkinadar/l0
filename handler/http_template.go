package handler

const (
	Template = `<!DOCTYPE html>
	<html>
	<head>
	   <title>Order Info</title>
	</head>
	<body>
	   <h1>Order Info</h1>
	
	   <form action="/order" method="POST">
	       <label for="uid">Enter Order UID:</label>
	       <input type="text" id="uid" name="uid" required>
	       <button type="submit">Get Order</button>
	   </form>
	
		{{ if .Order.OrderUid }}
			<h1>Order Info</h1>
				<p><strong>Order UID:</strong> {{ .Order.OrderUid }}</p>
				<p><strong>Track Number:</strong> {{ .Order.TrackNumber }}</p>
				<p><strong>Entry:</strong> {{ .Order.Entry }}</p>
	
				<h2>Delivery Details</h2>
				<p><strong>Name:</strong> {{ .Order.Delivery.Name }}</p>
				<p><strong>Phone:</strong> {{ .Order.Delivery.Phone }}</p>
				<p><strong>Zip:</strong> {{ .Order.Delivery.Zip }}</p>
				<p><strong>City:</strong> {{ .Order.Delivery.City }}</p>
				<p><strong>Address:</strong> {{ .Order.Delivery.Address }}</p>
				<p><strong>Region:</strong> {{ .Order.Delivery.Region }}</p>
				<p><strong>Email:</strong> {{ .Order.Delivery.Email }}</p>
	
				<h2>Payment Details</h2>
				<p><strong>Transaction:</strong> {{ .Order.Payment.Transaction }}</p>
				<p><strong>Request ID:</strong> {{ .Order.Payment.RequestID }}</p>
				<p><strong>Currency:</strong> {{ .Order.Payment.Currency }}</p>
				<p><strong>Provider:</strong> {{ .Order.Payment.Provider }}</p>
				<p><strong>Amount:</strong> {{ .Order.Payment.Amount }}</p>
				<p><strong>Payment Date:</strong> {{ .Order.Payment.PaymentDt }}</p>
				<p><strong>Bank:</strong> {{ .Order.Payment.Bank }}</p>
				<p><strong>Delivery Cost:</strong> {{ .Order.Payment.DeliveryCost }}</p>
				<p><strong>Goods Total:</strong> {{ .Order.Payment.GoodsTotal }}</p>
				<p><strong>Custom Fee:</strong> {{ .Order.Payment.CustomFee }}</p>
	
				<h2>Items</h2>
				<ul>
					{{ range .Order.Items }}
						<li>
							<p><strong>ChrtID:</strong> {{ .ChrtID }}</p>
							<p><strong>Track Number:</strong> {{ .TrackNumber }}</p>
							<p><strong>Price:</strong> {{ .Price }}</p>
							<p><strong>Rid:</strong> {{ .Rid }}</p>
							<p><strong>Name:</strong> {{ .Name }}</p>
							<p><strong>Sale:</strong> {{ .Sale }}</p>
							<p><strong>Size:</strong> {{ .Size }}</p>
							<p><strong>Total Price:</strong> {{ .TotalPrice }}</p>
							<p><strong>NmID:</strong> {{ .NmID }}</p>
							<p><strong>Brand:</strong> {{ .Brand }}</p>
							<p><strong>Status:</strong> {{ .Status }}</p>
						</li>
					{{ end }}
				</ul>
	
				<p><strong>Locale:</strong> {{ .Order.Locale }}</p>
				<p><strong>Internal Signature:</strong> {{ .Order.InternalSignature }}</p>
				<p><strong>Customer ID:</strong> {{ .Order.CustomerID }}</p>
				<p><strong>Delivery Service:</strong> {{ .Order.DeliveryService }}</p>
				<p><strong>Shardkey:</strong> {{ .Order.Shardkey }}</p>
				<p><strong>SmID:</strong> {{ .Order.SmID }}</p>
				<p><strong>Date Created:</strong> {{ .Order.DateCreated }}</p>
				<p><strong>Oof Shard:</strong> {{ .Order.OofShard }}</p>
	    {{ end }}
		{{ if .Error }}
			<h2> {{ .Error }} </h2>
		{{ end }}
	</body>
	</html>
`
)

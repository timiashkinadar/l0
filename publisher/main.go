package main

import (
	"log"
	"os"

	"github.com/nats-io/stan.go"
)

func main() {
	sc, err := stan.Connect("lo-cluster", "test-app")
	if err != nil {
		log.Fatal(err)
	}
	defer sc.Close()

	bytes, err := os.ReadFile("./publisher/model.json")
	if err != nil {
		log.Fatal("Cannot read json file")
	}

	err = sc.Publish("orders", bytes)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Sending a message")
}

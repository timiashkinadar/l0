CREATE TABLE IF NOT EXISTS orders (
                                      id SERIAL PRIMARY KEY,
                                      order_uid VARCHAR(255) UNIQUE,
    track_number VARCHAR(255),
    entry VARCHAR(255),
    locale VARCHAR(255),
    internal_signature VARCHAR(255),
    customer_id VARCHAR(255),
    delivery_service VARCHAR(255),
    shardkey VARCHAR(255),
    sm_id BIGINT,
    date_created VARCHAR(255),
    oof_shard VARCHAR(255)
    );

CREATE TABLE IF NOT EXISTS deliveries (
                                          id SERIAL PRIMARY KEY,
                                          order_id INT,
                                          name VARCHAR(255),
    phone VARCHAR(255),
    zip VARCHAR(255),
    city VARCHAR(255),
    address VARCHAR(255),
    region VARCHAR(255),
    email VARCHAR(255),
    FOREIGN KEY (order_id) REFERENCES orders (id)
    );

CREATE TABLE IF NOT EXISTS items (
                                     id SERIAL PRIMARY KEY,
                                     order_id INT,
                                     chrt_id BIGINT,
                                     track_number VARCHAR(255),
    price BIGINT,
    rid VARCHAR(255),
    name VARCHAR(255),
    sale BIGINT,
    size VARCHAR(255),
    total_price BIGINT,
    nm_id BIGINT,
    brand VARCHAR(255),
    status BIGINT,
    FOREIGN KEY (order_id) REFERENCES orders (id)
    );

CREATE TABLE IF NOT EXISTS payments (
                                        id SERIAL PRIMARY KEY,
                                        order_id INT,
                                        transaction VARCHAR(255),
    request_id VARCHAR(255),
    currency VARCHAR(255),
    provider VARCHAR(255),
    amount BIGINT,
    payment_dt BIGINT,
    bank VARCHAR(255),
    delivery_cost BIGINT,
    goods_total BIGINT,
    custom_fee BIGINT,
    FOREIGN KEY (order_id) REFERENCES orders (id)
    );


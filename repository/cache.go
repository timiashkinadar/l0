package repository

import (
	"fmt"
	"gitlab.com/timiashkinadar/l0/model"
	"sync"
)

type Cache interface {
	GetByID(UID string) (model.Order, error)
	SaveOrder(order model.Order)
}

type CacheOrder struct {
	orderMap map[string]*model.Order
	sync.RWMutex
}

func NewCache(repo Repository) (*CacheOrder, error) {
	m := make(map[string]*model.Order, 100)

	orders, err := repo.GetAggregatedInfo()
	if err != nil {
		return nil, err
	}

	cache := &CacheOrder{orderMap: m}

	for _, order := range orders {
		cache.SaveOrder(order)
	}

	return cache, nil
}

func (с *CacheOrder) GetByID(UID string) (model.Order, error) {
	с.RLock()
	defer с.RUnlock()

	if v, ok := с.orderMap[UID]; ok {
		return *v, nil
	}

	return model.Order{}, fmt.Errorf("order with id %s not found", UID)
}

func (с *CacheOrder) SaveOrder(order model.Order) {
	с.Lock()
	defer с.Unlock()

	с.orderMap[order.OrderUid] = &order
}

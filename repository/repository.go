package repository

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/georgysavva/scany/v2/sqlscan"
	"github.com/lib/pq"
	"gitlab.com/timiashkinadar/l0/config"
	"gitlab.com/timiashkinadar/l0/model"
	"log"
)

type Repository interface {
	SaveOrder(order model.Order) error
	GetItemsByOrderID(orderID int64) ([]model.Item, error)
	GetAggregatedInfo() ([]model.Order, error)
}

type Repo struct {
	db *sql.DB
}

type AggregatedInfo struct {
	Id int64 `db:"id" json:"id"`
	model.Order
	model.Delivery
	model.Payment
}

func NewRepository(cfg *config.Cfg) (Repository, *sql.DB) {
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", cfg.Postgres.Host, cfg.Postgres.Port, cfg.Postgres.User, cfg.Postgres.Passwd, cfg.Postgres.DbName)
	db, err := sql.Open("postgres", psqlconn)
	CheckError(err)

	err = db.Ping()
	CheckError(err)
	log.Println("Connected!")

	return &Repo{db: db}, db
}

func (r *Repo) SaveOrder(order model.Order) (err error) {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		}
	}()

	orderQuery := `
		INSERT INTO orders (order_uid, track_number, entry, locale, internal_signature, customer_id, delivery_service, shardkey, sm_id, date_created, oof_shard)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
		RETURNING id
	`

	var orderID int64
	err = tx.QueryRow(orderQuery, order.OrderUid, order.TrackNumber, order.Entry, order.Locale, order.InternalSignature, order.CustomerID, order.DeliveryService, order.Shardkey, order.SmID, order.DateCreated, order.OofShard).Scan(&orderID)
	if err != nil {
		return err
	}

	deliveryQuery := `
		INSERT INTO deliveries (order_id, name, phone, zip, city, address, region, email)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
	`

	delivery := order.Delivery
	_, err = tx.Exec(deliveryQuery, orderID, delivery.Name, delivery.Phone, delivery.Zip, delivery.City, delivery.Address, delivery.Region, delivery.Email)
	if err != nil {
		return err
	}

	paymentQuery := `
		INSERT INTO payments (order_id, transaction, request_id, currency, provider, amount, payment_dt, bank, delivery_cost, goods_total, custom_fee)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
	`

	payment := order.Payment
	_, err = tx.Exec(paymentQuery, orderID, payment.Transaction, payment.RequestID, payment.Currency, payment.Provider, payment.Amount, payment.PaymentDt, payment.Bank, payment.DeliveryCost, payment.GoodsTotal, payment.CustomFee)
	if err != nil {
		return err
	}

	itemQuery := `
		INSERT INTO items (order_id, chrt_id, track_number, price, rid, name, sale, size, total_price, nm_id, brand, status)
		SELECT $1, unnest($2::bigint[]), unnest($3::text[]), unnest($4::bigint[]), unnest($5::text[]), unnest($6::text[]), unnest($7::bigint[]), unnest($8::text[]), unnest($9::bigint[]), unnest($10::bigint[]), unnest($11::text[]), unnest($12::bigint[])
	`

	items := order.Items
	var chrtIDs, trackNumbers, prices, rids, names, sales, sizes, totalPrices, nmIDs, brands, statuses []interface{}
	for _, item := range items {
		chrtIDs = append(chrtIDs, item.ChrtID)
		trackNumbers = append(trackNumbers, item.TrackNumber)
		prices = append(prices, item.Price)
		rids = append(rids, item.Rid)
		names = append(names, item.Name)
		sales = append(sales, item.Sale)
		sizes = append(sizes, item.Size)
		totalPrices = append(totalPrices, item.TotalPrice)
		nmIDs = append(nmIDs, item.NmID)
		brands = append(brands, item.Brand)
		statuses = append(statuses, item.Status)
	}

	_, err = tx.Exec(itemQuery, orderID, pq.Array(chrtIDs), pq.Array(trackNumbers), pq.Array(prices), pq.Array(rids), pq.Array(names), pq.Array(sales), pq.Array(sizes), pq.Array(totalPrices), pq.Array(nmIDs), pq.Array(brands), pq.Array(statuses))
	if err != nil {
		return err
	}

	tx.Commit()

	return nil
}

func (r *Repo) GetItemsByOrderID(orderID int64) ([]model.Item, error) {
	query := `
		SELECT i.chrt_id, i.track_number, i.price, i.rid, i.name, i.sale, i.size, i.total_price, i.nm_id, i.brand, i.status
		FROM items i
		WHERE i.order_id = $1
	`

	var items []model.Item
	err := sqlscan.Select(context.Background(), r.db, &items, query, orderID)
	if err != nil {
		return nil, err
	}

	return items, nil
}

func (r *Repo) GetAggregatedInfo() (orders []model.Order, err error) {
	query := `
		SELECT o.id, o.order_uid, o.track_number, o.entry, o.locale, o.internal_signature, o.customer_id, o.delivery_service, o.shardkey, o.sm_id, o.date_created, o.oof_shard,
		       d.name, d.phone, d.zip, d.city, d.address, d.region, d.email,
		       p.transaction, p.request_id, p.currency, p.provider, p.amount, p.payment_dt, p.bank, p.delivery_cost, p.goods_total, p.custom_fee
		FROM orders as o
		INNER JOIN deliveries as d ON o.id = d.order_id
		INNER JOIN payments as p ON o.id = p.order_id
	`

	rows, err := r.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var aggregatedInfo []AggregatedInfo

	err = sqlscan.Select(context.Background(), r.db, &aggregatedInfo, query)
	if err != nil {
		return nil, err
	}

	for _, info := range aggregatedInfo {
		order := info.Order
		order.Delivery = info.Delivery
		order.Payment = info.Payment

		order.Items, err = r.GetItemsByOrderID(info.Id)
		if err != nil {
			return nil, err
		}

		orders = append(orders, order)
	}

	return orders, nil
}

func CheckError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

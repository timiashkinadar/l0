package model

import "encoding/json"

func UnmarshalOrder(data []byte) (Order, error) {
	var r Order
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Order) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Order struct {
	OrderUid          string   `db:"order_uid" json:"order_uid"`
	TrackNumber       string   `db:"track_number" json:"track_number"`
	Entry             string   `db:"entry" json:"entry"`
	Delivery          Delivery `db:"delivery" json:"delivery"`
	Payment           Payment  `db:"payment" json:"payment"`
	Items             []Item   `db:"items" json:"items"`
	Locale            string   `db:"locale" json:"locale"`
	InternalSignature string   `db:"internal_signature" json:"internal_signature"`
	CustomerID        string   `db:"customer_id" json:"customer_id"`
	DeliveryService   string   `db:"delivery_service" json:"delivery_service"`
	Shardkey          string   `db:"shardkey" json:"shardkey"`
	SmID              int64    `db:"sm_id" json:"sm_id"`
	DateCreated       string   `db:"date_created" json:"date_created"`
	OofShard          string   `db:"oof_shard" json:"oof_shard"`
}

type Delivery struct {
	Name    string `db:"name" json:"name"`
	Phone   string `db:"phone" json:"phone"`
	Zip     string `db:"zip" json:"zip"`
	City    string `db:"city" json:"city"`
	Address string `db:"address" json:"address"`
	Region  string `db:"region" json:"region"`
	Email   string `db:"email" json:"email"`
}

type Item struct {
	ChrtID      int64  `db:"chrt_id" json:"chrt_id"`
	TrackNumber string `db:"track_number" json:"track_number"`
	Price       int64  `db:"price" json:"price"`
	Rid         string `db:"rid" json:"rid"`
	Name        string `db:"name" json:"name"`
	Sale        int64  `db:"sale" json:"sale"`
	Size        string `db:"size" json:"size"`
	TotalPrice  int64  `db:"total_price" json:"total_price"`
	NmID        int64  `db:"nm_id" json:"nm_id"`
	Brand       string `db:"brand" json:"brand"`
	Status      int64  `db:"status" json:"status"`
}

type Payment struct {
	Transaction  string `db:"transaction" json:"transaction"`
	RequestID    string `db:"request_id" json:"request_id"`
	Currency     string `db:"currency" json:"currency"`
	Provider     string `db:"provider" json:"provider"`
	Amount       int64  `db:"amount" json:"amount"`
	PaymentDt    int64  `db:"payment_dt" json:"payment_dt"`
	Bank         string `db:"bank" json:"bank"`
	DeliveryCost int64  `db:"delivery_cost" json:"delivery_cost"`
	GoodsTotal   int64  `db:"goods_total" json:"goods_total"`
	CustomFee    int64  `db:"custom_fee" json:"custom_fee"`
}
